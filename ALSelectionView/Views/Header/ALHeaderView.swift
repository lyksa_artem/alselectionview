//
//  PopUpHeaderView.swift
//  iBankKredo-Test
//
//  Created by Artem Lyksa on 6/21/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

public final class ALHeaderView: UIView {
    
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var button: UIButton!
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var leftConstraint: NSLayoutConstraint!
    
    public var config: ALHeaderConfig? {
        didSet {
            if let config = config {
                configure(with: config)
            }
        }
    }
    
    private func configure(with model: ALHeaderConfig) {
        label.font = model.font
        label.text = model.title
        
        imageView.tintColor = model.tintColor
        imageView.image = model.closeImage
        button.isEnabled = model.closeImage != nil
        
        heightConstraint.constant = model.height
        leftConstraint.constant = model.titleOffset
    }
    
    public func hide() {
        heightConstraint.constant = 0.0
    }
}
