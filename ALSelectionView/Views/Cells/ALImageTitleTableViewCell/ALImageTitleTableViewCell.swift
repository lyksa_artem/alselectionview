//
//  ALImageTitleTableViewCell.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 8/5/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit

class ALImageTitleTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var itemImageView: UIImageView!
    @IBOutlet private weak var accessoryImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var leftConstraint: NSLayoutConstraint!
    
    private var config: ALCellConfig? {
        didSet {
            guard let _config = config else { return }
            titleLabel.font = _config.font
            accessoryImageView.tintColor = _config.tintColor
            heightConstraint.constant = _config.height
            leftConstraint.constant = _config.titleOffset
            titleLabel.textColor = _config.titleColor
            backgroundColor = _config.backgroundColor
        }
    }
    
    func configure(with model: ALImageTitleItem) {
        itemImageView.image = model.image
        accessoryImageView.image = model.accessoryImage
        config = model.config
        if let attrTitle = model.attrTitle {
            titleLabel.attributedText = attrTitle
        } else {
            titleLabel.text = model.title
        }
    }
}
