//
//  SelectionTableViewCell.swift
//  iBankKredo-Test
//
//  Created by Artem Lyksa on 6/21/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

class ALTitleTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var accessoryImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet private weak var accessoryWidth: NSLayoutConstraint!
    
    private var config: ALCellConfig? {
        didSet {
            guard let _config = config else { return }
            titleLabel.font = _config.font
            accessoryImageView.tintColor = _config.tintColor
            heightConstraint.constant = _config.height
            leftConstraint.constant = _config.titleOffset
            titleLabel.textAlignment = _config.titleAlignment
            titleLabel.textColor = _config.titleColor
            backgroundColor = _config.backgroundColor
            selectionStyle = _config.selectionStyle
        }
    }

    func configure(with model: ALTitleItem) {
        accessoryImageView.image = model.accessoryImage
        config = model.config
        
        if let attrTitle = model.attrTitle {
            titleLabel.attributedText = attrTitle
        } else {
            titleLabel.text = model.title
        }
        
        if model.accessoryImage == nil {
            accessoryWidth.constant = 0.0
        }
    }
    
    func configure(with model: ALSelectionItem) {
        accessoryImageView.image = model.accessoryImage
        config = model.config
        
        if let attrTitle = model.attrTitle {
            titleLabel.attributedText = attrTitle
        } else {
            titleLabel.text = model.title
        }
        
        if model.accessoryImage == nil {
            accessoryWidth.constant = 0.0
        }
    }
    
}
