//
//  SelfSizedTableView.swift
//  iBankKredo
//
//  Created by Artem Lyksa on 7/19/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

protocol LayoutDelegate: class {
    func didUpdateHeight(_ value: CGFloat)
}

class SelfSizedTableView: UITableView {
    
    weak var layoutDelegate: LayoutDelegate?
    
    override var contentSize: CGSize {
        set {
            super.contentSize = newValue
            layoutDelegate?.didUpdateHeight(newValue.height)
        }
        get {
            return super.contentSize
        }
    }

}
