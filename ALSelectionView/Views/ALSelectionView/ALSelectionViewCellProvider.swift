//
//  ALSelectionViewCellProvider.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 8/3/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation

public protocol ALSelectionViewCellProvider {
    
    func selectionTableView(_ selectionTableView: UITableView,
                            cellForRowAt indexPath: IndexPath,
                            item: ALSelectionItem) -> UITableViewCell
}
