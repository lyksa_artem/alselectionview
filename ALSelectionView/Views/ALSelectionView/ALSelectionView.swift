//
//  PopUpView.swift
//  iBankKredo-Test
//
//  Created by Artem Lyksa on 6/21/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

public typealias ALSelectionItemHandler = ((ALSelectionItem) -> Void)
public typealias ALSelectionResultHandler = ((Int) -> Void)
public typealias ALSelectionCloseHandler = (() -> Void)
public typealias ALSelectionWillCloseHandler = (() -> Void)
public typealias ALSelectionLayouUpdatedHandler = (() -> Void)

public class ALSelectionView: UIView {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var tableView: SelfSizedTableView!
    @IBOutlet private weak var headerView: ALHeaderView!
    @IBOutlet private weak var widthConstraint: NSLayoutConstraint!
    
    private var heightConstraint: NSLayoutConstraint?
    private var anchorContraints: (x: NSLayoutConstraint, y: NSLayoutConstraint)?
    
    private var handleInput: ALSelectionResultHandler?
    private var handleItem: ALSelectionItemHandler?
    private var closeHandler: ALSelectionCloseHandler?
    private var willCloseHandler: ALSelectionCloseHandler?
    
    private var originalDataSource: [ALSelectionItem] = []
    private var dataSource: [ALSelectionItem] = []
    private var appearance: ALAppearance!
    private var config: ALLayoutConfig!
    private var cellProvider: ALSelectionViewCellProvider?
    
    public var popUpFrame: CGRect {
        return containerView.frame
    }
    
    public var layoutUpdatedHandler: ALSelectionLayouUpdatedHandler?
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupViewsOnLoad()
    }
    
    public override func removeFromSuperview() {
        tableView.layoutDelegate = nil

        if config.direction == .vertical {
            heightConstraint?.constant = 0.0
        } else {
            widthConstraint.constant = 0.0
        }
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.alpha = 0.0
            self?.superview?.layoutIfNeeded()
        }) { _ in
            super.removeFromSuperview()
        }
    }

    public override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if !containerView.frame.contains(point) {
            willCloseHandler?()
            removeFromSuperview()
            closeHandler?()
        }
        
        return super.point(inside: point, with: event)
    }
    
    private func setupViewsOnLoad() {
        layer.cornerRadius = 5.0
        containerView.dropShadow()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 38.0
    }
    
    @IBAction private func closeButtonAction() {
        willCloseHandler?()
        removeFromSuperview()
        closeHandler?()
    }
    
    @discardableResult
    public static func show(config: ALLayoutConfig,
                            dataSource: [ALSelectionItem],
                            appearance: ALAppearance,
                            cellProvider: ALSelectionViewCellProvider? = nil,
                            handler: @escaping ALSelectionResultHandler,
                            closeHandler: ALSelectionCloseHandler? = nil,
                            willCloseHandler: ALSelectionWillCloseHandler? = nil) -> ALSelectionView {
        
        if let selectionView = config.superview.subviews.first(where: { $0 is ALSelectionView }) as? ALSelectionView {
            return selectionView
        }
        
        let view = ALSelectionView.fromNib()
        view.originalDataSource = dataSource
        view.dataSource = dataSource
        view.appearance = appearance
        view.handleInput = handler
        view.closeHandler = closeHandler
        view.willCloseHandler = willCloseHandler
        view.config = config
        view.cellProvider = cellProvider
        config.superview.addSubview(view)
        view.configure(with: config)
        
        return view
    }
    
    @discardableResult
    public static func show(config: ALLayoutConfig,
                            dataSource: [ALSelectionItem],
                            appearance: ALAppearance,
                            cellProvider: ALSelectionViewCellProvider? = nil,
                            itemHandler: @escaping ALSelectionItemHandler,
                            closeHandler: ALSelectionCloseHandler? = nil,
                            willCloseHandler: ALSelectionWillCloseHandler? = nil) -> ALSelectionView {
        
        let view = ALSelectionView.fromNib()
        view.originalDataSource = dataSource
        view.dataSource = dataSource
        view.appearance = appearance
        view.handleInput = nil
        view.handleItem = itemHandler
        view.closeHandler = closeHandler
        view.willCloseHandler = willCloseHandler
        view.config = config
        view.cellProvider = cellProvider
        config.superview.addSubview(view)
        view.configure(with: config)
        
        return view
    }
    
    private func configure(with config: ALLayoutConfig) {
        
        alpha = 0.0
        translatesAutoresizingMaskIntoConstraints = false
        
        if let config = appearance?.headerConfig {
            headerView?.config = config
        } else {
            headerView.hide()
        }
        
        leftAnchor.constraint(equalTo: config.superview.leftAnchor).isActive = true
        rightAnchor.constraint(equalTo: config.superview.rightAnchor).isActive = true
        topAnchor.constraint(equalTo: config.superview.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: config.superview.bottomAnchor).isActive = true
        heightConstraint = containerView.heightAnchor.constraint(equalToConstant: 60)
        heightConstraint?.isActive = true
        configureContainer(config: config)
    }
    
    private func configureContainer(config: ALLayoutConfig) {
        let myAnchor = containerView.anchorPoint(for: config.selectionViewCorner)
        let anchorViewAnchor = config.view.anchorPoint(for: config.viewCorner)
        anchorContraints = myAnchor.connect(with: anchorViewAnchor)
        
        if config.direction != .horizontal {
            let myoppAnchor = containerView.oppositeAnchorPoint(for: config.selectionViewCorner)
            let anchoroppViewAnchor = oppositeAnchorPoint(for: config.selectionViewCorner)
            
            if config.isToBottom {
                myoppAnchor.connectOpp(with: anchoroppViewAnchor)
            } else {
                anchoroppViewAnchor.connectOpp(with: myoppAnchor)
            }
        }

        //Opposite
        superview?.layoutIfNeeded()
        
        tableView.layoutDelegate = self
        tableView.reloadData()
    }
    
    public func filter(by keyword: String?) {
        guard let keyword = keyword, !keyword.isEmpty else {
            unfilter(); return
        }
        
        dataSource = originalDataSource.filter {
            $0.title.validKeyword.contains(keyword.validKeyword)
        }
        tableView.reloadData()
    }
    
    public func unfilter() {
        dataSource = originalDataSource
        tableView.reloadData()
    }
    
    public func set(dataSource: [ALSelectionItem]) {
        self.dataSource = dataSource
        tableView.reloadData()
    }
}

extension ALSelectionView: LayoutDelegate {
    
    func didUpdateHeight(_ value: CGFloat) {
        var width = config.superview.frame.size.width
        let height = config.height ?? (value + (headerView.config?.height ?? 0.0))
        
        if config.isWidthAutoAdjusted {
            if config.selectionViewCorner == .topRight || config.selectionViewCorner == .bottomRight {
                let delta = superview!.frame.width - containerView.frame.maxX
                width -= delta * 2.0
            }
            
            if config.selectionViewCorner == .topLeft || config.selectionViewCorner == .bottomLeft {
                let delta = containerView.frame.minX
                width -= delta * 2.0
            }
            
        }
        
        if config.direction == .vertical {
            //In case of vertical direction we don't need width animation
            widthConstraint.constant = config.width ?? width
            
            superview?.layoutIfNeeded()
            let newHeight = min(height, self.frame.size.height)
            heightConstraint?.constant = newHeight
            if config.lessThanOrEqualHeight {
                heightConstraint?.constant = min(newHeight, value)
            }
        }
        
        if config.direction == .horizontal {
            //No need to animate height if direction is horizontal
            heightConstraint?.constant = height
            superview?.layoutIfNeeded()
            
            widthConstraint.constant = config.width ?? width
        }
        
        layoutUpdatedHandler?()
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.alpha = 1.0
            self?.superview?.layoutIfNeeded()
        }
    }
    
}

extension ALSelectionView: UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = cellProvider?.selectionTableView(tableView, cellForRowAt: indexPath, item: dataSource[indexPath.row]) {
            return cell
        }
        
        let model = dataSource[indexPath.row]
        
        if let titleItem = model as? ALTitleItem {
            let cell = tableView.getCell(ofType: ALTitleTableViewCell.self)
            cell.configure(with: titleItem)
            return cell
        }
        
        if let imageTitleItem = model as? ALImageTitleItem {
            let cell = tableView.getCell(ofType: ALImageTitleTableViewCell.self)
            cell.configure(with: imageTitleItem)
            return cell
        }

        let cell = tableView.getCell(ofType: ALTitleTableViewCell.self)
        cell.configure(with: model)
        return cell
        
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        handleInput?(indexPath.row)
        handleItem?(dataSource[indexPath.row])
        
        if appearance.hideOnSelection {
            removeFromSuperview()
        }
    }
}
