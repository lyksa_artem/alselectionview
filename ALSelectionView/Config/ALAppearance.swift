//
//  Appearance.swift
//  iBankKredo-Test
//
//  Created by Artem Lyksa on 6/21/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

public struct ALAppearance {
    
    let headerConfig: ALHeaderConfig?
    let hideOnSelection: Bool
    
    public init(headerConfig: ALHeaderConfig? = nil,
                hideOnSelection: Bool = true) {
        
        self.headerConfig = headerConfig
        self.hideOnSelection = hideOnSelection
    }
}

