//
//  PopUpAnchor.swift
//  iBankKredo-Test
//
//  Created by Artem Lyksa on 6/21/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

public enum Direction {
    case horizontal
    case vertical
}

public enum AnchorCorner {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
    case center
}

public class ALLayoutConfig {
    
    let selectionViewCorner: AnchorCorner
    let viewCorner: AnchorCorner
    let direction: Direction
    
    ///Specify width of view
    ///If nil - width will be equals to the width of a superview
    let width: CGFloat?
    
    ///Specify height of view
    ///If nil - height will be calculated automatically based on content size
    let height: CGFloat?
    
    ///Anchor view
    let view: UIView
    
    ///Super view
    let superview: UIView
    
    var isWidthAutoAdjusted: Bool
    var widthDelta: CGFloat = 0.0
    var lessThanOrEqualHeight = false
    
    var isToBottom: Bool {
        if (selectionViewCorner == .topLeft || selectionViewCorner == .topRight) && direction == .vertical {
            return true
        }
        return false
    }
    
    public init(selectionViewCorner: AnchorCorner,
                viewCorner: AnchorCorner,
                direction: Direction,
                width: CGFloat? = nil,
                height: CGFloat? = nil,
                lessThanOrEqualHeight: Bool = false,
                view: UIView,
                isWidthAutoAdjusted: Bool = true,
                superview: UIView) {
        
        self.selectionViewCorner = selectionViewCorner
        self.viewCorner = viewCorner
        self.direction = direction
        self.width = width
        self.height = height
        self.lessThanOrEqualHeight = lessThanOrEqualHeight
        self.view = view
        self.isWidthAutoAdjusted = isWidthAutoAdjusted
        self.superview = superview
    }
}
