//
//  ALAnchorPoint.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 8/3/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation

struct AnchorPoint {

    let x: NSLayoutXAxisAnchor
    let y: NSLayoutYAxisAnchor
    
    @discardableResult
    func connect(with point: AnchorPoint, xOffset: CGFloat = 0.0, yOffset: CGFloat = 0.0) -> (x: NSLayoutConstraint, y: NSLayoutConstraint) {
        
        let xConstraint = self.x.constraint(equalTo: point.x, constant: xOffset)
        xConstraint.priority = UILayoutPriority(999.0)
        xConstraint.isActive = true
        
        let yConstraint = self.y.constraint(equalTo: point.y, constant: yOffset)
        yConstraint.priority = UILayoutPriority(999.0)
        yConstraint.isActive = true
        
        return (x: xConstraint, y: yConstraint)
    }
    
    func connectOpp(with point: AnchorPoint, xOffset: CGFloat = 0.0, yOffset: CGFloat = 0.0) {
        
        let yConstraint = self.y.constraint(lessThanOrEqualTo: point.y, constant: yOffset)
        yConstraint.isActive = true
    }
}
