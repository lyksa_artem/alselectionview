//
//  ALHeaderConfig.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 8/3/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit

public struct ALHeaderConfig {
    
    let title: String
    let closeImage: UIImage?
    let font: UIFont
    let tintColor: UIColor
    let height: CGFloat
    let titleOffset: CGFloat
    
    public init(title: String ,
                closeImage: UIImage? = nil,
                font: UIFont = .systemFont(ofSize: 15.0),
                tintColor: UIColor = .blue,
                height: CGFloat = 50.0,
                titleOffset: CGFloat = 16.0) {
        
        self.title = title
        self.closeImage = closeImage
        self.font = font
        self.tintColor = tintColor
        self.height = height
        self.titleOffset = titleOffset
    }
}

