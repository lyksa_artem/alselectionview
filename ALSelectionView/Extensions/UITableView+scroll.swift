//
//  UITableViewExtension.swift
//  iBankKredo
//
//  Created by Evgeniy Romanishin on 4/17/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    func scrollToBottom() {
        let maxOffset = contentSize.height - frame.height
        if maxOffset <= 0 { return }
        setContentOffset(CGPoint(x: contentOffset.x, y: maxOffset), animated: true)
    }
    
    func scrollToTop() {
        setContentOffset(CGPoint(x: contentOffset.x, y: 0.0), animated: true)
    }
    
}

extension UITableView {
    
    func getCell<T>(ofType type: T.Type) -> T {
        
        let nibName = "\(type)"
        
        let bundle = Bundle(for: type as! AnyClass)
        guard let cell = dequeueReusableCell(withIdentifier: nibName) as? T else {
            register(UINib(nibName: nibName, bundle: bundle), forCellReuseIdentifier: nibName)
            return dequeueReusableCell(withIdentifier: nibName) as! T
        }
        
        return cell
    }
    
    func getHeaderFooter<T>(ofType type: T.Type) -> T {
        
        let nibName = "\(type)"
        
        guard let headerFooter = dequeueReusableHeaderFooterView(withIdentifier: nibName) as? T else {
            register(UINib(nibName: nibName, bundle: nil), forHeaderFooterViewReuseIdentifier: nibName)
            return dequeueReusableHeaderFooterView(withIdentifier: nibName) as! T
        }
        
        return headerFooter
    }
}
