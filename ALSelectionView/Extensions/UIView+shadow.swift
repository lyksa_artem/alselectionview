//
//  UIView+shadow.swift
//  iBankKredo-Test
//
//  Created by Artem Lyksa on 6/7/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

extension UIView {
    
    func dropShadow(opacity: Float = 0.2) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = CGSize.zero
    }
    
}
