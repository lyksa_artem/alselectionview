//
//  String+filter.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 6/6/22.
//

import Foundation

extension String {
    
    var validKeyword: String {
        return lowercased().replacingOccurrences(of: " ", with: "")
    }
    
}
