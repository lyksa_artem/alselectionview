//
//  UIView+xib.swift
//  iBankKredo-Test
//
//  Created by Artem Lyksa on 6/11/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

protocol NibInitializable {
    static func fromNib() -> Self
}

extension NibInitializable where Self: UIView {
    
    static func fromNib() -> Self {
        let id = String(describing: self)
        return Bundle(for: Self.self).loadNibNamed(id, owner: nil)![0] as! Self
    }
}

extension UIView: NibInitializable { }

