//
//  UIView+corners.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 8/3/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit

extension UIView {
    
    func anchorPoint(for corner: AnchorCorner) -> AnchorPoint {
        let xAcnhor = xAnchor(for: corner)
        let yAcnhor = yAnchor(for: corner)
        return AnchorPoint(x: xAcnhor, y: yAcnhor)
    }
    
    func oppositeAnchorPoint(for corner: AnchorCorner) -> AnchorPoint {
        let xAcnhor = oppositeXAnchor(for: corner)
        let yAcnhor = oppositeYAnchor(for: corner)
        return AnchorPoint(x: xAcnhor, y: yAcnhor)
    }
    
    func xAnchor(for corner: AnchorCorner) -> NSLayoutXAxisAnchor {
        switch corner {
        case .topLeft, .bottomLeft:
            return leftAnchor
        case .topRight, .bottomRight:
            return rightAnchor
        case .center:
            return centerXAnchor
        }
    }
    
    func yAnchor(for corner: AnchorCorner) -> NSLayoutYAxisAnchor {
        switch corner {
        case .topLeft, .topRight:
            return topAnchor
        case .bottomLeft, .bottomRight:
            return bottomAnchor
        case .center:
            return centerYAnchor
        }
    }
    
    func oppositeXAnchor(for corner: AnchorCorner) -> NSLayoutXAxisAnchor {
        switch corner {
        case .topLeft, .bottomLeft:
            return rightAnchor
        case .topRight, .bottomRight:
            return leftAnchor
        case .center:
            return centerXAnchor
        }
    }
    
    func oppositeYAnchor(for corner: AnchorCorner) -> NSLayoutYAxisAnchor {
        switch corner {
        case .topLeft, .topRight:
            return bottomAnchor
        case .bottomLeft, .bottomRight:
            return topAnchor
        case .center:
            return centerYAnchor
        }
    }
    
}
