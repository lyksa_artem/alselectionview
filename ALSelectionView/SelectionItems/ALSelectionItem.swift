//
//  ALSelectionItem.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 8/3/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation

public protocol ALSelectionItem {
    var title: String { get }
    var accessoryImage: UIImage? { get }
    var config: ALCellConfig? { get }
    var attrTitle: NSMutableAttributedString? { get }
}
