//
//  ALCellConfig.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 8/3/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation

public struct ALCellConfig {
    
    let tintColor: UIColor
    let font: UIFont
    let height: CGFloat
    let titleOffset: CGFloat
    let titleAlignment: NSTextAlignment
    let backgroundColor: UIColor
    let titleColor: UIColor
    let selectionStyle: UITableViewCell.SelectionStyle
    
    public init(tintColor: UIColor = .blue,
                font: UIFont = .systemFont(ofSize: 15.0),
                height: CGFloat = 44.0,
                titleOffset: CGFloat = 16.0,
                backgroundColor: UIColor = .white,
                titleColor: UIColor = .black,
                selectionStyle: UITableViewCell.SelectionStyle = .default ) {
        
        self.tintColor = tintColor
        self.font = font
        self.height = height
        self.titleOffset = titleOffset
        self.titleAlignment = .left
        self.backgroundColor = backgroundColor
        self.titleColor = titleColor
        self.selectionStyle = selectionStyle
    }
    
    public init(tintColor: UIColor = .blue,
                font: UIFont = .systemFont(ofSize: 15.0),
                height: CGFloat = 44.0,
                titleAlignment: NSTextAlignment,
                backgroundColor: UIColor = .white,
                titleColor: UIColor = .black,
                selectionStyle: UITableViewCell.SelectionStyle = .default) {
        
        self.tintColor = tintColor
        self.font = font
        self.height = height
        self.titleOffset = 0.0
        self.titleAlignment = titleAlignment
        self.backgroundColor = backgroundColor
        self.titleColor = titleColor
        self.selectionStyle = selectionStyle
    }
}
