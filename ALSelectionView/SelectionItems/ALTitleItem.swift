//
//  PopUpItem.swift
//  iBankKredo-Test
//
//  Created by Artem Lyksa on 6/21/19.
//  Copyright © 2019 KredoBank. All rights reserved.
//

import UIKit

public struct ALTitleItem: ALSelectionItem {
    
    public let title: String
    public let accessoryImage: UIImage?
    public let config: ALCellConfig?
    public let attrTitle: NSMutableAttributedString?
    
    public init(title: String, accessoryImage: UIImage? = nil, config: ALCellConfig? = nil, attrTitle: NSMutableAttributedString? = nil) {
        self.title = title
        self.accessoryImage = accessoryImage
        self.config = config
        self.attrTitle = attrTitle
    }
}
