//
//  ALImageTitleItem.swift
//  ALSelectionView
//
//  Created by Artem Lyksa on 8/5/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation

public struct ALImageTitleItem: ALSelectionItem {
    
    public let title: String
    public let image: UIImage
    public let accessoryImage: UIImage?
    public let config: ALCellConfig?
    public let attrTitle: NSMutableAttributedString?
    
    public init(title: String, image: UIImage, accessoryImage: UIImage? = nil, config: ALCellConfig? = nil, attrTitle: NSMutableAttributedString? = nil) {
        self.title = title
        self.image = image
        self.accessoryImage = accessoryImage
        self.config = config
        self.attrTitle = attrTitle
    }
}
